﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aukcija
{
    public partial class FormLicitacija : MetroFramework.Forms.MetroForm
    {
        public RedisDataLayer rdl = null;
        string username = "";
        Int32 staraCena;
        Int32 novaCena;
        List<Predmet> lista;
        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public FormLicitacija()
        {
            InitializeComponent();
        }

        private void FormLicitacija_Load(object sender, EventArgs e)
        {
            rdl = new RedisDataLayer();
            lblUser.Text = username;
            UcitajPredmeteUListu();
        }

        void UcitajPredmeteUListu()
        {
            lista = rdl.GetAllItems();

            if (lista.Count == 0)
            {
                gridPredmeta.Visible = false;
                lblLabela.Text = "Trenutno nema predmeta za licitiranje.";
            }
            else
            {
                gridPredmeta.Visible = true;
                lblLabela.Text = "";
                var source = new BindingSource();
                source.DataSource = lista;
                gridPredmeta.DataSource = source;
            }
        }

        private void gridPredmeta_Click(object sender, EventArgs e)
        {
            if (gridPredmeta.SelectedRows.Count == 0)
                return;

            int izabraniPredmet = gridPredmeta.SelectedRows[0].Index;
            Int32 id = (Int32)gridPredmeta["ID", izabraniPredmet].Value;

            Predmet predmet = rdl.GetItem(id);

            txtLicitacija.Text = predmet.Cena;
        }

        private void btnLicitiraj_Click(object sender, EventArgs e)
        {
            if (txtLicitacija.Text == "")
                return;

            if (gridPredmeta.SelectedRows.Count == 0)
                return;

            int izabraniPredmet = gridPredmeta.SelectedRows[0].Index;
            Int32 id = (Int32)gridPredmeta["ID", izabraniPredmet].Value;

            Predmet predmet = rdl.GetItem(id);

            staraCena = Int32.Parse(predmet.Cena);
            novaCena = Int32.Parse(txtLicitacija.Text);

            if (staraCena >= novaCena)
            {
                MessageBox.Show("Nova cena mora biti veca od stare cene.");
                return;
            }

            predmet.Cena = txtLicitacija.Text;
            predmet.PoslednjiLicitirao = username;

            if (rdl.UpdateItem(id, predmet))
            {
                MessageBox.Show("Uspesno licitirano za " + predmet.Naziv + ".");
                txtLicitacija.Text = "";
                UcitajPredmeteUListu();
            }
        }

        private void FormLicitacija_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void txtLicitacija_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }
    }
}
