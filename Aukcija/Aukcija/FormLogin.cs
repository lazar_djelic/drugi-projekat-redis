﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aukcija
{
    public partial class FormLogin : MetroFramework.Forms.MetroForm
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private void txtPrijava_Click(object sender, EventArgs e)
        {
 
        }

        private void btnReg_Click(object sender, EventArgs e)
        {
            this.Hide();
            var frmReg = new FormRegister();
            DialogResult dr = frmReg.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.Cancel)
            {
                this.Show();
            }

        }
    }
}
