﻿namespace Aukcija
{
    partial class FormGlavna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtKIme = new MetroFramework.Controls.MetroTextBox();
            this.txtLozinka = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.btnLogin = new MetroFramework.Controls.MetroButton();
            this.btnRegister = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(67, 69);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(96, 19);
            this.metroLabel1.TabIndex = 4;
            this.metroLabel1.Text = "Korisnicko ime:";
            // 
            // txtKIme
            // 
            // 
            // 
            // 
            this.txtKIme.CustomButton.Image = null;
            this.txtKIme.CustomButton.Location = new System.Drawing.Point(118, 1);
            this.txtKIme.CustomButton.Name = "";
            this.txtKIme.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtKIme.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtKIme.CustomButton.TabIndex = 1;
            this.txtKIme.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtKIme.CustomButton.UseSelectable = true;
            this.txtKIme.CustomButton.Visible = false;
            this.txtKIme.Lines = new string[0];
            this.txtKIme.Location = new System.Drawing.Point(69, 91);
            this.txtKIme.MaxLength = 32767;
            this.txtKIme.Name = "txtKIme";
            this.txtKIme.PasswordChar = '\0';
            this.txtKIme.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtKIme.SelectedText = "";
            this.txtKIme.SelectionLength = 0;
            this.txtKIme.SelectionStart = 0;
            this.txtKIme.ShortcutsEnabled = true;
            this.txtKIme.Size = new System.Drawing.Size(140, 23);
            this.txtKIme.TabIndex = 0;
            this.txtKIme.UseSelectable = true;
            this.txtKIme.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtKIme.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtLozinka
            // 
            // 
            // 
            // 
            this.txtLozinka.CustomButton.Image = null;
            this.txtLozinka.CustomButton.Location = new System.Drawing.Point(118, 1);
            this.txtLozinka.CustomButton.Name = "";
            this.txtLozinka.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtLozinka.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtLozinka.CustomButton.TabIndex = 1;
            this.txtLozinka.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtLozinka.CustomButton.UseSelectable = true;
            this.txtLozinka.CustomButton.Visible = false;
            this.txtLozinka.Lines = new string[0];
            this.txtLozinka.Location = new System.Drawing.Point(69, 139);
            this.txtLozinka.MaxLength = 25;
            this.txtLozinka.Name = "txtLozinka";
            this.txtLozinka.PasswordChar = '●';
            this.txtLozinka.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLozinka.SelectedText = "";
            this.txtLozinka.SelectionLength = 0;
            this.txtLozinka.SelectionStart = 0;
            this.txtLozinka.ShortcutsEnabled = true;
            this.txtLozinka.Size = new System.Drawing.Size(140, 23);
            this.txtLozinka.TabIndex = 1;
            this.txtLozinka.UseSelectable = true;
            this.txtLozinka.UseSystemPasswordChar = true;
            this.txtLozinka.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtLozinka.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(67, 117);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(55, 19);
            this.metroLabel2.TabIndex = 5;
            this.metroLabel2.Text = "Lozinka:";
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(95, 186);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(85, 23);
            this.btnLogin.TabIndex = 2;
            this.btnLogin.Text = "Prijavite se";
            this.btnLogin.UseSelectable = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(95, 225);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(85, 23);
            this.btnRegister.TabIndex = 3;
            this.btnRegister.Text = "Registrujte se";
            this.btnRegister.UseSelectable = true;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // FormGlavna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(263, 280);
            this.Controls.Add(this.btnRegister);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.txtLozinka);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.txtKIme);
            this.Controls.Add(this.metroLabel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormGlavna";
            this.Text = "Prijava";
            this.Load += new System.EventHandler(this.FormGlavna_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtKIme;
        private MetroFramework.Controls.MetroTextBox txtLozinka;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroButton btnLogin;
        private MetroFramework.Controls.MetroButton btnRegister;

    }
}