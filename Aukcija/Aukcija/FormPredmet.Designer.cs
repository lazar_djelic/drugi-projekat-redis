﻿namespace Aukcija
{
    partial class FormPredmet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblNaziv = new MetroFramework.Controls.MetroLabel();
            this.lblOpis = new MetroFramework.Controls.MetroLabel();
            this.lblCena = new MetroFramework.Controls.MetroLabel();
            this.txtNaziv = new MetroFramework.Controls.MetroTextBox();
            this.predmetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtOpis = new MetroFramework.Controls.MetroTextBox();
            this.txtCena = new MetroFramework.Controls.MetroTextBox();
            this.btnDodaj = new MetroFramework.Controls.MetroButton();
            this.btnObrisi = new MetroFramework.Controls.MetroButton();
            this.btnIzmeni = new MetroFramework.Controls.MetroButton();
            this.lblUser = new MetroFramework.Controls.MetroLabel();
            this.gridPredmeta = new MetroFramework.Controls.MetroGrid();
            this.lblLabela = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.predmetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPredmeta)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNaziv
            // 
            this.lblNaziv.AutoSize = true;
            this.lblNaziv.Location = new System.Drawing.Point(11, 84);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(106, 19);
            this.lblNaziv.TabIndex = 7;
            this.lblNaziv.Text = "Naziv predmeta:";
            // 
            // lblOpis
            // 
            this.lblOpis.AutoSize = true;
            this.lblOpis.Location = new System.Drawing.Point(78, 113);
            this.lblOpis.Name = "lblOpis";
            this.lblOpis.Size = new System.Drawing.Size(39, 19);
            this.lblOpis.TabIndex = 8;
            this.lblOpis.Text = "Opis:";
            // 
            // lblCena
            // 
            this.lblCena.AutoSize = true;
            this.lblCena.Location = new System.Drawing.Point(75, 143);
            this.lblCena.Name = "lblCena";
            this.lblCena.Size = new System.Drawing.Size(42, 19);
            this.lblCena.TabIndex = 9;
            this.lblCena.Text = "Cena:";
            // 
            // txtNaziv
            // 
            // 
            // 
            // 
            this.txtNaziv.CustomButton.Image = null;
            this.txtNaziv.CustomButton.Location = new System.Drawing.Point(158, 1);
            this.txtNaziv.CustomButton.Name = "";
            this.txtNaziv.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNaziv.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNaziv.CustomButton.TabIndex = 1;
            this.txtNaziv.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNaziv.CustomButton.UseSelectable = true;
            this.txtNaziv.CustomButton.Visible = false;
            this.txtNaziv.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.predmetBindingSource, "Naziv", true));
            this.txtNaziv.Lines = new string[0];
            this.txtNaziv.Location = new System.Drawing.Point(123, 84);
            this.txtNaziv.MaxLength = 32767;
            this.txtNaziv.Name = "txtNaziv";
            this.txtNaziv.PasswordChar = '\0';
            this.txtNaziv.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNaziv.SelectedText = "";
            this.txtNaziv.SelectionLength = 0;
            this.txtNaziv.SelectionStart = 0;
            this.txtNaziv.ShortcutsEnabled = true;
            this.txtNaziv.Size = new System.Drawing.Size(180, 23);
            this.txtNaziv.TabIndex = 0;
            this.txtNaziv.UseSelectable = true;
            this.txtNaziv.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNaziv.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // predmetBindingSource
            // 
            this.predmetBindingSource.DataSource = typeof(Aukcija.Predmet);
            // 
            // txtOpis
            // 
            // 
            // 
            // 
            this.txtOpis.CustomButton.Image = null;
            this.txtOpis.CustomButton.Location = new System.Drawing.Point(158, 1);
            this.txtOpis.CustomButton.Name = "";
            this.txtOpis.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtOpis.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtOpis.CustomButton.TabIndex = 1;
            this.txtOpis.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtOpis.CustomButton.UseSelectable = true;
            this.txtOpis.CustomButton.Visible = false;
            this.txtOpis.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.predmetBindingSource, "Opis", true));
            this.txtOpis.Lines = new string[0];
            this.txtOpis.Location = new System.Drawing.Point(123, 113);
            this.txtOpis.MaxLength = 32767;
            this.txtOpis.Name = "txtOpis";
            this.txtOpis.PasswordChar = '\0';
            this.txtOpis.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtOpis.SelectedText = "";
            this.txtOpis.SelectionLength = 0;
            this.txtOpis.SelectionStart = 0;
            this.txtOpis.ShortcutsEnabled = true;
            this.txtOpis.Size = new System.Drawing.Size(180, 23);
            this.txtOpis.TabIndex = 1;
            this.txtOpis.UseSelectable = true;
            this.txtOpis.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtOpis.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtCena
            // 
            // 
            // 
            // 
            this.txtCena.CustomButton.Image = null;
            this.txtCena.CustomButton.Location = new System.Drawing.Point(104, 1);
            this.txtCena.CustomButton.Name = "";
            this.txtCena.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtCena.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCena.CustomButton.TabIndex = 1;
            this.txtCena.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCena.CustomButton.UseSelectable = true;
            this.txtCena.CustomButton.Visible = false;
            this.txtCena.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.predmetBindingSource, "Cena", true));
            this.txtCena.Lines = new string[0];
            this.txtCena.Location = new System.Drawing.Point(123, 143);
            this.txtCena.MaxLength = 32767;
            this.txtCena.Name = "txtCena";
            this.txtCena.PasswordChar = '\0';
            this.txtCena.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCena.SelectedText = "";
            this.txtCena.SelectionLength = 0;
            this.txtCena.SelectionStart = 0;
            this.txtCena.ShortcutsEnabled = true;
            this.txtCena.Size = new System.Drawing.Size(126, 23);
            this.txtCena.TabIndex = 2;
            this.txtCena.UseSelectable = true;
            this.txtCena.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCena.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtCena.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCena_KeyPress);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(380, 187);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(156, 23);
            this.btnDodaj.TabIndex = 3;
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.UseSelectable = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(461, 432);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(75, 23);
            this.btnObrisi.TabIndex = 6;
            this.btnObrisi.Text = "Obrisi";
            this.btnObrisi.UseSelectable = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // btnIzmeni
            // 
            this.btnIzmeni.Location = new System.Drawing.Point(380, 432);
            this.btnIzmeni.Name = "btnIzmeni";
            this.btnIzmeni.Size = new System.Drawing.Size(75, 23);
            this.btnIzmeni.TabIndex = 5;
            this.btnIzmeni.Text = "Izmeni";
            this.btnIzmeni.UseSelectable = true;
            this.btnIzmeni.Click += new System.EventHandler(this.btnIzmeni_Click);
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(501, 31);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(35, 19);
            this.lblUser.TabIndex = 10;
            this.lblUser.Text = "User";
            // 
            // gridPredmeta
            // 
            this.gridPredmeta.AllowUserToAddRows = false;
            this.gridPredmeta.AllowUserToDeleteRows = false;
            this.gridPredmeta.AllowUserToResizeColumns = false;
            this.gridPredmeta.AllowUserToResizeRows = false;
            this.gridPredmeta.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridPredmeta.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridPredmeta.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridPredmeta.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridPredmeta.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridPredmeta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridPredmeta.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridPredmeta.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gridPredmeta.EnableHeadersVisualStyles = false;
            this.gridPredmeta.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gridPredmeta.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridPredmeta.Location = new System.Drawing.Point(11, 226);
            this.gridPredmeta.MultiSelect = false;
            this.gridPredmeta.Name = "gridPredmeta";
            this.gridPredmeta.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridPredmeta.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gridPredmeta.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridPredmeta.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gridPredmeta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridPredmeta.Size = new System.Drawing.Size(528, 189);
            this.gridPredmeta.TabIndex = 4;
            this.gridPredmeta.Click += new System.EventHandler(this.gridPredmeta_Click);
            // 
            // lblLabela
            // 
            this.lblLabela.AutoSize = true;
            this.lblLabela.Location = new System.Drawing.Point(23, 204);
            this.lblLabela.Name = "lblLabela";
            this.lblLabela.Size = new System.Drawing.Size(0, 0);
            this.lblLabela.TabIndex = 11;
            // 
            // FormPredmet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 478);
            this.Controls.Add(this.lblLabela);
            this.Controls.Add(this.gridPredmeta);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.btnIzmeni);
            this.Controls.Add(this.btnObrisi);
            this.Controls.Add(this.btnDodaj);
            this.Controls.Add(this.txtCena);
            this.Controls.Add(this.txtOpis);
            this.Controls.Add(this.txtNaziv);
            this.Controls.Add(this.lblCena);
            this.Controls.Add(this.lblOpis);
            this.Controls.Add(this.lblNaziv);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormPredmet";
            this.Text = "Upravljaj predmetima";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPredmet_FormClosing);
            this.Load += new System.EventHandler(this.FormPredmet_Load);
            ((System.ComponentModel.ISupportInitialize)(this.predmetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPredmeta)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblNaziv;
        private MetroFramework.Controls.MetroLabel lblOpis;
        private MetroFramework.Controls.MetroLabel lblCena;
        private MetroFramework.Controls.MetroTextBox txtNaziv;
        private MetroFramework.Controls.MetroTextBox txtOpis;
        private MetroFramework.Controls.MetroTextBox txtCena;
        private MetroFramework.Controls.MetroButton btnDodaj;
        private MetroFramework.Controls.MetroButton btnObrisi;
        private MetroFramework.Controls.MetroButton btnIzmeni;
        private System.Windows.Forms.BindingSource predmetBindingSource;
        private MetroFramework.Controls.MetroLabel lblUser;
        private MetroFramework.Controls.MetroGrid gridPredmeta;
        private MetroFramework.Controls.MetroLabel lblLabela;
    }
}

