﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aukcija.Server;
using ServiceStack.Redis;

namespace Aukcija
{
    public class RedisDataLayer
    {
        #region Fields

        readonly static RedisClient redis = new RedisClient(Config.SingleHost);

        private const string maxItemIDString = "global:maxItemID";

        private const string userListString = "global:users";
        private const string auctionListString = "global:auctions";


        #endregion

        #region Properties

        public int MaxItemID
        {
            get
            {
                string entry = maxItemIDString;
                int maxID = -1;

                // Proveri da li postoji podatak sacuvaj u bazi
                if (redis.Exists(entry) != 0)
                    maxID = redis.Get<int>(entry);

                return maxID;
            }
        }

        #endregion

        #region Korisnik

        public bool AddUser(string username, string password)
        {
            bool success = false;
            string entry = UserEntryString(username);

            // Ako korisnik ne postoji, dodaj ga
            if (redis.Exists(entry) == 0)
            {
                redis.Set(entry, password);
                redis.PushItemToList(userListString, entry);
                success = true;
            }

            return success;
        }

        public bool CheckUser(string username, string password)
        {
            bool success = false;
            string entry = UserEntryString(username);

            // Ako korisnik postoji, proveri da li se lozinke podudaraju
            if (redis.Exists(entry) != 0)
            {
                string pass = redis.Get<string>(entry);
                if (pass == password)
                    success = true;
            }

            return success;
        }

        #endregion

        #region Predmet

        public bool AddItem(Predmet item)
        {
            bool success = false;

            try
            {
                item.ID = IncrementItemID();
                string entry = ItemEntryString(item.ID);
                redis.Set(entry, item.ToString());
                redis.PushItemToList(auctionListString, item.ID.ToString());
                success = true;
            }
            catch (Exception _) { /* Ne radimo nista sa izuzetkom. Samo vratimo false */ }

            return success;
        }

        public bool RemoveItem(int id)
        {
            bool success = false;

            try
            {
                if (!IsItemOnAuction(id))
                    throw new Exception("Nije moguce ukloniti predmet koji nije na aukciji.");

                redis.RemoveItemFromList(auctionListString, id.ToString()); // Ukloni iz liste aukcija
                redis.Remove(ItemEntryString(id)); // Ukloni individualni predmet
                success = true;
            }
            catch (Exception _) { /* Ne radimo nista sa izuzetkom. Samo vratimo false */ }

            return success;
        }

        public Predmet GetItem(int id)
        {
            Predmet item = null;

            try
            {
                if (!IsItemOnAuction(id))
                    throw new Exception("Nije moguce pribaviti predmet koji nije na aukciji");

                string entry = ItemEntryString(id);
                string podaci = redis.Get<string>(entry);
                item = new Predmet();
                item.FromString(podaci);
            }
            catch (Exception _) { /* Ne radimo nista sa izuzetkom. Samo vratimo null */ }

            return item;
        }

        public List<Predmet> GetAllItems()
        {
            List<Predmet> items = new List<Predmet>();
            try
            {
                var onAuctionIds = redis.GetAllItemsFromList(auctionListString).Select(x => int.Parse(x)).ToList();
                foreach (int id in onAuctionIds)
                    items.Add(GetItem(id));
            }
            catch (Exception _) { /* Ako nastane izuzetak, ne radimo nista. */}
            return items;
        }

        public bool UpdateItem(int id, Predmet item)
        {
            bool success = false;
            try
            {
                if (!IsItemOnAuction(id))
                    throw new Exception("Nije moguce azurirati predmet koji nije na aukciji.");

                string entry = ItemEntryString(id);
                item.ID = id;
                redis.Set(entry, item.ToString());

                success = true;
            }
            catch (Exception _) { /* Ne radimo nista sa izuzetkom. Samo vratimo false */ }

            return success;
        }

        #endregion

        #region Ostalo

        /// <summary>
        /// Vraca identifikacioni string za odredjenog korisnika. Na ovoj lokaciji je smestena njegova sifra.
        /// </summary>
        private string UserEntryString(string username)
        {
            return "user:" + username + ":password";
        }

        /// <summary>
        /// Vraca identifikacioni string za predmet na aukciji. Na ovoj lokaciji su smesteni podaci o predmetu.
        /// </summary>
        private string ItemEntryString(int id)
        {
            return "item:" + id + ":data";
        }

        /// <summary>
        /// Inkrementuje i vraca id predmeta kako bi mogao da bude dodat u bazu
        /// </summary>
        private int IncrementItemID()
        {
            // Dobavi max id iz baze. Za slucaj da ne postoji, vratice se -1,
            // sto ce biti inkrementovano na 0, i sacuvano u bazu.
            int maxID = MaxItemID + 1;

            // Azuriraj u bazi novi max id
            redis.Set(maxItemIDString, maxID);

            // Vrati novu inkrementovanu vrednost
            return maxID;
        }

        private bool IsItemOnAuction(int id)
        {
            var list = redis.GetAllItemsFromList(auctionListString);
            if (list.Contains(id.ToString()))
                return true;
            return false;
        }

        #endregion

    }
}