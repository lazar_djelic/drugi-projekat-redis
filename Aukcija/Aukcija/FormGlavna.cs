﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aukcija
{
    public partial class FormGlavna : MetroFramework.Forms.MetroForm
    {
        public RedisDataLayer rdl = null;

        public FormGlavna()
        {
            InitializeComponent();
        }

        private void FormGlavna_Load(object sender, EventArgs e)
        {
            rdl = new RedisDataLayer();
        }

        private bool proveriLoginData()
        {
            if (String.IsNullOrWhiteSpace(txtKIme.Text))
            {
                MessageBox.Show("Korisnicko ime ne sme biti prazno.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (String.IsNullOrWhiteSpace(txtLozinka.Text))
            {
                MessageBox.Show("Lozinka ne sme biti prazna.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            bool status = rdl.CheckUser(txtKIme.Text, txtLozinka.Text);
            if (!status)
                MessageBox.Show("Korisnicko ime i lozinka se ne podudaraju.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return status;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (!proveriLoginData())
                return;

            DialogResult dr;
            if (txtKIme.Text == "admin") 
            {
                FormPredmet frmPredmet = new FormPredmet();
                frmPredmet.Username = txtKIme.Text;
                this.Visible = false;
                dr = frmPredmet.ShowDialog();
            }
            else
            {
                FormLicitacija frmLicitacija = new FormLicitacija();
                frmLicitacija.Username = txtKIme.Text;
                this.Visible = false;
                dr = frmLicitacija.ShowDialog();
            }

            if (dr == DialogResult.OK || dr == DialogResult.Cancel)
            {
                txtKIme.Text = "";
                txtLozinka.Text = "";
                this.Visible = true;
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            FormRegister frmReg = new FormRegister();
            this.Visible = false;
            DialogResult dr = frmReg.ShowDialog();
            if (dr == DialogResult.OK || dr == DialogResult.Cancel)
            {
                this.Visible = true;
            }
        }
    }
}
