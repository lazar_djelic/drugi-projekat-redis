﻿namespace Aukcija
{
    partial class FormLicitacija
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtLicitacija = new MetroFramework.Controls.MetroTextBox();
            this.btnLicitiraj = new MetroFramework.Controls.MetroButton();
            this.lblUser = new MetroFramework.Controls.MetroLabel();
            this.gridPredmeta = new MetroFramework.Controls.MetroGrid();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.lblLabela = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.gridPredmeta)).BeginInit();
            this.SuspendLayout();
            // 
            // txtLicitacija
            // 
            // 
            // 
            // 
            this.txtLicitacija.CustomButton.Image = null;
            this.txtLicitacija.CustomButton.Location = new System.Drawing.Point(129, 1);
            this.txtLicitacija.CustomButton.Name = "";
            this.txtLicitacija.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtLicitacija.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtLicitacija.CustomButton.TabIndex = 1;
            this.txtLicitacija.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtLicitacija.CustomButton.UseSelectable = true;
            this.txtLicitacija.CustomButton.Visible = false;
            this.txtLicitacija.Lines = new string[0];
            this.txtLicitacija.Location = new System.Drawing.Point(121, 94);
            this.txtLicitacija.MaxLength = 32767;
            this.txtLicitacija.Name = "txtLicitacija";
            this.txtLicitacija.PasswordChar = '\0';
            this.txtLicitacija.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLicitacija.SelectedText = "";
            this.txtLicitacija.SelectionLength = 0;
            this.txtLicitacija.SelectionStart = 0;
            this.txtLicitacija.ShortcutsEnabled = true;
            this.txtLicitacija.Size = new System.Drawing.Size(151, 23);
            this.txtLicitacija.TabIndex = 0;
            this.txtLicitacija.UseSelectable = true;
            this.txtLicitacija.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtLicitacija.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtLicitacija.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLicitacija_KeyPress);
            // 
            // btnLicitiraj
            // 
            this.btnLicitiraj.Location = new System.Drawing.Point(297, 94);
            this.btnLicitiraj.Name = "btnLicitiraj";
            this.btnLicitiraj.Size = new System.Drawing.Size(75, 23);
            this.btnLicitiraj.TabIndex = 1;
            this.btnLicitiraj.Text = "Licitiraj";
            this.btnLicitiraj.UseSelectable = true;
            this.btnLicitiraj.Click += new System.EventHandler(this.btnLicitiraj_Click);
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(581, 29);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(35, 19);
            this.lblUser.TabIndex = 4;
            this.lblUser.Text = "User";
            // 
            // gridPredmeta
            // 
            this.gridPredmeta.AllowUserToAddRows = false;
            this.gridPredmeta.AllowUserToDeleteRows = false;
            this.gridPredmeta.AllowUserToResizeColumns = false;
            this.gridPredmeta.AllowUserToResizeRows = false;
            this.gridPredmeta.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridPredmeta.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridPredmeta.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridPredmeta.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridPredmeta.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridPredmeta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridPredmeta.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridPredmeta.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gridPredmeta.EnableHeadersVisualStyles = false;
            this.gridPredmeta.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gridPredmeta.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridPredmeta.Location = new System.Drawing.Point(23, 153);
            this.gridPredmeta.MultiSelect = false;
            this.gridPredmeta.Name = "gridPredmeta";
            this.gridPredmeta.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridPredmeta.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gridPredmeta.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridPredmeta.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gridPredmeta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridPredmeta.Size = new System.Drawing.Size(593, 234);
            this.gridPredmeta.TabIndex = 2;
            this.gridPredmeta.Click += new System.EventHandler(this.gridPredmeta_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(23, 94);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(92, 19);
            this.metroLabel1.TabIndex = 3;
            this.metroLabel1.Text = "Nova ponuda:";
            // 
            // lblLabela
            // 
            this.lblLabela.AutoSize = true;
            this.lblLabela.Location = new System.Drawing.Point(23, 131);
            this.lblLabela.Name = "lblLabela";
            this.lblLabela.Size = new System.Drawing.Size(0, 0);
            this.lblLabela.TabIndex = 5;
            // 
            // FormLicitacija
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 410);
            this.Controls.Add(this.lblLabela);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.gridPredmeta);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.btnLicitiraj);
            this.Controls.Add(this.txtLicitacija);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormLicitacija";
            this.Text = "Aukcija";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormLicitacija_FormClosing);
            this.Load += new System.EventHandler(this.FormLicitacija_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridPredmeta)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox txtLicitacija;
        private MetroFramework.Controls.MetroButton btnLicitiraj;
        private MetroFramework.Controls.MetroLabel lblUser;
        private MetroFramework.Controls.MetroGrid gridPredmeta;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel lblLabela;
    }
}