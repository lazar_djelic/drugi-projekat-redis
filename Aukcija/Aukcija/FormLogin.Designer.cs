﻿namespace Aukcija
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIme = new MetroFramework.Controls.MetroLabel();
            this.lblLozinka = new MetroFramework.Controls.MetroLabel();
            this.txtIme = new MetroFramework.Controls.MetroTextBox();
            this.txtLozinka = new MetroFramework.Controls.MetroTextBox();
            this.txtPrijava = new MetroFramework.Controls.MetroButton();
            this.btnReg = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // lblIme
            // 
            this.lblIme.AutoSize = true;
            this.lblIme.Location = new System.Drawing.Point(23, 77);
            this.lblIme.Name = "lblIme";
            this.lblIme.Size = new System.Drawing.Size(96, 19);
            this.lblIme.TabIndex = 0;
            this.lblIme.Text = "Korisnicko ime:";
            // 
            // lblLozinka
            // 
            this.lblLozinka.AutoSize = true;
            this.lblLozinka.Location = new System.Drawing.Point(64, 106);
            this.lblLozinka.Name = "lblLozinka";
            this.lblLozinka.Size = new System.Drawing.Size(55, 19);
            this.lblLozinka.TabIndex = 1;
            this.lblLozinka.Text = "Lozinka:";
            // 
            // txtIme
            // 
            // 
            // 
            // 
            this.txtIme.CustomButton.Image = null;
            this.txtIme.CustomButton.Location = new System.Drawing.Point(151, 1);
            this.txtIme.CustomButton.Name = "";
            this.txtIme.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtIme.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtIme.CustomButton.TabIndex = 1;
            this.txtIme.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtIme.CustomButton.UseSelectable = true;
            this.txtIme.CustomButton.Visible = false;
            this.txtIme.Lines = new string[0];
            this.txtIme.Location = new System.Drawing.Point(125, 73);
            this.txtIme.MaxLength = 32767;
            this.txtIme.Name = "txtIme";
            this.txtIme.PasswordChar = '\0';
            this.txtIme.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIme.SelectedText = "";
            this.txtIme.SelectionLength = 0;
            this.txtIme.SelectionStart = 0;
            this.txtIme.ShortcutsEnabled = true;
            this.txtIme.Size = new System.Drawing.Size(173, 23);
            this.txtIme.TabIndex = 2;
            this.txtIme.UseSelectable = true;
            this.txtIme.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtIme.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtLozinka
            // 
            // 
            // 
            // 
            this.txtLozinka.CustomButton.Image = null;
            this.txtLozinka.CustomButton.Location = new System.Drawing.Point(151, 1);
            this.txtLozinka.CustomButton.Name = "";
            this.txtLozinka.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtLozinka.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtLozinka.CustomButton.TabIndex = 1;
            this.txtLozinka.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtLozinka.CustomButton.UseSelectable = true;
            this.txtLozinka.CustomButton.Visible = false;
            this.txtLozinka.Lines = new string[0];
            this.txtLozinka.Location = new System.Drawing.Point(125, 102);
            this.txtLozinka.MaxLength = 32767;
            this.txtLozinka.Name = "txtLozinka";
            this.txtLozinka.PasswordChar = '\0';
            this.txtLozinka.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLozinka.SelectedText = "";
            this.txtLozinka.SelectionLength = 0;
            this.txtLozinka.SelectionStart = 0;
            this.txtLozinka.ShortcutsEnabled = true;
            this.txtLozinka.Size = new System.Drawing.Size(173, 23);
            this.txtLozinka.TabIndex = 3;
            this.txtLozinka.UseSelectable = true;
            this.txtLozinka.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtLozinka.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtPrijava
            // 
            this.txtPrijava.Location = new System.Drawing.Point(125, 154);
            this.txtPrijava.Name = "txtPrijava";
            this.txtPrijava.Size = new System.Drawing.Size(75, 23);
            this.txtPrijava.TabIndex = 4;
            this.txtPrijava.Text = "Prijavi se";
            this.txtPrijava.UseSelectable = true;
            this.txtPrijava.Click += new System.EventHandler(this.txtPrijava_Click);
            // 
            // btnReg
            // 
            this.btnReg.Location = new System.Drawing.Point(223, 154);
            this.btnReg.Name = "btnReg";
            this.btnReg.Size = new System.Drawing.Size(75, 23);
            this.btnReg.TabIndex = 5;
            this.btnReg.Text = "Registruj se";
            this.btnReg.UseSelectable = true;
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            // 
            // FormLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 194);
            this.Controls.Add(this.btnReg);
            this.Controls.Add(this.txtPrijava);
            this.Controls.Add(this.txtLozinka);
            this.Controls.Add(this.txtIme);
            this.Controls.Add(this.lblLozinka);
            this.Controls.Add(this.lblIme);
            this.Name = "FormLogin";
            this.Text = "Prijavite se";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblIme;
        private MetroFramework.Controls.MetroLabel lblLozinka;
        private MetroFramework.Controls.MetroTextBox txtIme;
        private MetroFramework.Controls.MetroTextBox txtLozinka;
        private MetroFramework.Controls.MetroButton txtPrijava;
        private MetroFramework.Controls.MetroButton btnReg;
    }
}