﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aukcija
{
    public class Predmet
    {
        public int ID { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public string Cena { get; set; }
        public string PoslednjiLicitirao { get; set; }

        public override string ToString()
        {
            return "id:" + ID + ";naziv:" + Naziv + ";opis:" + Opis + ";cena:" + Cena + ";aukcioner:" + PoslednjiLicitirao;
        }

        /// <summary>
        /// Ucitava podatke u predmet iz prethodno ispravno formatiranog stringa
        /// </summary>
        public void FromString(string data)
        {
            string[] podaci = data.Split(';');
            foreach (string podatak in podaci)
            {
                string[] kv = podatak.Split(':');
                if (kv[0] == "id")
                    ID = int.Parse(kv[1]);
                else if (kv[0] == "naziv")
                    Naziv = kv[1];
                else if (kv[0] == "opis")
                    Opis = kv[1];
                else if (kv[0] == "cena")
                    Cena = kv[1];
                else if (kv[0] == "aukcioner")
                    PoslednjiLicitirao = kv[1];
            }
        }
    }
}