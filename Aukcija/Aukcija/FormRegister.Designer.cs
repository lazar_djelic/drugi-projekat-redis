﻿namespace Aukcija
{
    partial class FormRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.btnRegistracija = new MetroFramework.Controls.MetroButton();
            this.txtKIme = new MetroFramework.Controls.MetroTextBox();
            this.txtLoz = new MetroFramework.Controls.MetroTextBox();
            this.txtPotvrda = new MetroFramework.Controls.MetroTextBox();
            this.btnNazad = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(23, 77);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(96, 19);
            this.metroLabel1.TabIndex = 5;
            this.metroLabel1.Text = "Korisnicko ime:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(64, 105);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(55, 19);
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "Lozinka:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(17, 133);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(101, 19);
            this.metroLabel3.TabIndex = 7;
            this.metroLabel3.Text = "Potvrda lozinke:";
            // 
            // btnRegistracija
            // 
            this.btnRegistracija.Location = new System.Drawing.Point(141, 170);
            this.btnRegistracija.Name = "btnRegistracija";
            this.btnRegistracija.Size = new System.Drawing.Size(89, 23);
            this.btnRegistracija.TabIndex = 3;
            this.btnRegistracija.Text = "Registrujte se";
            this.btnRegistracija.UseSelectable = true;
            this.btnRegistracija.Click += new System.EventHandler(this.btnRegistracija_Click);
            // 
            // txtKIme
            // 
            // 
            // 
            // 
            this.txtKIme.CustomButton.Image = null;
            this.txtKIme.CustomButton.Location = new System.Drawing.Point(164, 1);
            this.txtKIme.CustomButton.Name = "";
            this.txtKIme.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtKIme.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtKIme.CustomButton.TabIndex = 1;
            this.txtKIme.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtKIme.CustomButton.UseSelectable = true;
            this.txtKIme.CustomButton.Visible = false;
            this.txtKIme.Lines = new string[0];
            this.txtKIme.Location = new System.Drawing.Point(125, 72);
            this.txtKIme.MaxLength = 32767;
            this.txtKIme.Name = "txtKIme";
            this.txtKIme.PasswordChar = '\0';
            this.txtKIme.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtKIme.SelectedText = "";
            this.txtKIme.SelectionLength = 0;
            this.txtKIme.SelectionStart = 0;
            this.txtKIme.ShortcutsEnabled = true;
            this.txtKIme.Size = new System.Drawing.Size(186, 23);
            this.txtKIme.TabIndex = 0;
            this.txtKIme.UseSelectable = true;
            this.txtKIme.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtKIme.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtLoz
            // 
            // 
            // 
            // 
            this.txtLoz.CustomButton.Image = null;
            this.txtLoz.CustomButton.Location = new System.Drawing.Point(164, 1);
            this.txtLoz.CustomButton.Name = "";
            this.txtLoz.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtLoz.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtLoz.CustomButton.TabIndex = 1;
            this.txtLoz.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtLoz.CustomButton.UseSelectable = true;
            this.txtLoz.CustomButton.Visible = false;
            this.txtLoz.Lines = new string[0];
            this.txtLoz.Location = new System.Drawing.Point(125, 101);
            this.txtLoz.MaxLength = 32767;
            this.txtLoz.Name = "txtLoz";
            this.txtLoz.PasswordChar = '●';
            this.txtLoz.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLoz.SelectedText = "";
            this.txtLoz.SelectionLength = 0;
            this.txtLoz.SelectionStart = 0;
            this.txtLoz.ShortcutsEnabled = true;
            this.txtLoz.Size = new System.Drawing.Size(186, 23);
            this.txtLoz.TabIndex = 1;
            this.txtLoz.UseSelectable = true;
            this.txtLoz.UseSystemPasswordChar = true;
            this.txtLoz.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtLoz.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtPotvrda
            // 
            // 
            // 
            // 
            this.txtPotvrda.CustomButton.Image = null;
            this.txtPotvrda.CustomButton.Location = new System.Drawing.Point(164, 1);
            this.txtPotvrda.CustomButton.Name = "";
            this.txtPotvrda.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPotvrda.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPotvrda.CustomButton.TabIndex = 1;
            this.txtPotvrda.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPotvrda.CustomButton.UseSelectable = true;
            this.txtPotvrda.CustomButton.Visible = false;
            this.txtPotvrda.Lines = new string[0];
            this.txtPotvrda.Location = new System.Drawing.Point(125, 129);
            this.txtPotvrda.MaxLength = 32767;
            this.txtPotvrda.Name = "txtPotvrda";
            this.txtPotvrda.PasswordChar = '●';
            this.txtPotvrda.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPotvrda.SelectedText = "";
            this.txtPotvrda.SelectionLength = 0;
            this.txtPotvrda.SelectionStart = 0;
            this.txtPotvrda.ShortcutsEnabled = true;
            this.txtPotvrda.Size = new System.Drawing.Size(186, 23);
            this.txtPotvrda.TabIndex = 2;
            this.txtPotvrda.UseSelectable = true;
            this.txtPotvrda.UseSystemPasswordChar = true;
            this.txtPotvrda.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPotvrda.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnNazad
            // 
            this.btnNazad.Location = new System.Drawing.Point(236, 170);
            this.btnNazad.Name = "btnNazad";
            this.btnNazad.Size = new System.Drawing.Size(75, 23);
            this.btnNazad.TabIndex = 4;
            this.btnNazad.Text = "Nazad";
            this.btnNazad.UseSelectable = true;
            this.btnNazad.Click += new System.EventHandler(this.btnNazad_Click);
            // 
            // FormRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 216);
            this.Controls.Add(this.btnNazad);
            this.Controls.Add(this.txtPotvrda);
            this.Controls.Add(this.txtLoz);
            this.Controls.Add(this.txtKIme);
            this.Controls.Add(this.btnRegistracija);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormRegister";
            this.Text = "Registracija";
            this.Load += new System.EventHandler(this.FormRegister_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroButton btnRegistracija;
        private MetroFramework.Controls.MetroTextBox txtKIme;
        private MetroFramework.Controls.MetroTextBox txtLoz;
        private MetroFramework.Controls.MetroTextBox txtPotvrda;
        private MetroFramework.Controls.MetroButton btnNazad;
    }
}