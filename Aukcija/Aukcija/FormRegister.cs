﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aukcija
{
    public partial class FormRegister : MetroFramework.Forms.MetroForm
    {
        public RedisDataLayer rdl = null;

        public FormRegister()
        {
            InitializeComponent();
        }

        private bool checkInput()
        {
            bool isOkay = true;

            if (String.IsNullOrWhiteSpace(txtKIme.Text))
            {
                MessageBox.Show("Polje za korisnicko ime ne sme biti prazno.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                isOkay = false;
            }
            else if (String.IsNullOrWhiteSpace(txtLoz.Text))
            {
                MessageBox.Show("Polje za lozinku ne sme biti prazno.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                isOkay = false;
            }
            else if (String.IsNullOrWhiteSpace(txtPotvrda.Text))
            {
                MessageBox.Show("Molimo vas da potvrdite vasu lozinku.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                isOkay = false;
            }
            else if (txtLoz.Text != txtPotvrda.Text)
            {
                MessageBox.Show("Unesene lozinke se ne podudaraju.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                isOkay = false;
            }
            return isOkay;
        }

        private void FormRegister_Load(object sender, EventArgs e)
        {
            rdl = new RedisDataLayer();
        }

        private void btnNazad_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnRegistracija_Click(object sender, EventArgs e)
        {
            if (!checkInput())
                return;
            bool suc = false;
            suc = rdl.AddUser(txtKIme.Text, txtLoz.Text);
            if (suc)
            {
                MessageBox.Show("Nalog uspesno kreiran!");
                DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                MessageBox.Show("Korisnicko ime je vec u upotrebi.");
        }
    }
}
