﻿using ServiceStack.Redis;
using ServiceStack.Redis.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aukcija
{
    public partial class FormPredmet : MetroFramework.Forms.MetroForm
    {
        public RedisDataLayer rdl = null;
        List<Predmet> lista;
        string username = "";
        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public FormPredmet()
        {
            InitializeComponent();
        }

        void UcitajPredmeteUListu()
        {
            lista = rdl.GetAllItems();

            if (lista.Count == 0)
            {
                gridPredmeta.Visible = false;
                lblLabela.Text = "Trenutno nema predmeta za licitiranje.";
            }
            else
            {
                gridPredmeta.Visible = true;
                lblLabela.Text = "";
                var source = new BindingSource();
                source.DataSource = lista;
                gridPredmeta.DataSource = source;

                gridPredmeta.Columns[0].Width = 85;
            }
        }

        private void FormPredmet_Load(object sender, EventArgs e)
        {
            rdl = new RedisDataLayer();
            lblUser.Text = username;
            UcitajPredmeteUListu();
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            if (gridPredmeta.SelectedRows.Count == 0)
                return;

            int izabraniPredmet = gridPredmeta.SelectedRows[0].Index;
            Int32 id = (Int32)gridPredmeta["ID", izabraniPredmet].Value;

            Predmet predmet = new Predmet();
            predmet.ID = id;
            predmet.Naziv = txtNaziv.Text;
            predmet.Opis = txtOpis.Text;
            predmet.Cena = txtCena.Text;
            predmet.PoslednjiLicitirao = "";

            if (rdl.UpdateItem(id, predmet))
            {
                MessageBox.Show("Predmet uspesno azuriran.");
                ocistiPolja();
                UcitajPredmeteUListu();
            }
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (gridPredmeta.SelectedRows.Count == 0)
                return;

            int izabraniPredmet = gridPredmeta.SelectedRows[0].Index;
            Int32 id = (Int32)gridPredmeta["ID", izabraniPredmet].Value;

            if (rdl.RemoveItem(id))
            {
                MessageBox.Show("Predmet uspesno obrisan.");
                ocistiPolja();
                UcitajPredmeteUListu();
            }
        }


        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (ProveriPolja())
            {
                Predmet predmet = new Predmet();
                predmet.Naziv = txtNaziv.Text;
                predmet.Opis = txtOpis.Text;
                predmet.Cena = txtCena.Text;

                if (rdl.AddItem(predmet))
                    MessageBox.Show("Predmet dodat u listu.");

                ocistiPolja();
                UcitajPredmeteUListu();
            }
            else
                MessageBox.Show("Morate uneti sve vrednosti u polja.");
        }

        bool ProveriPolja()
        {
            if (txtNaziv.Text == "" || txtOpis.Text == "" || txtCena.Text == "")
                return false;

            return true;
        }

        void ocistiPolja()
        {
            txtNaziv.Text = "";
            txtOpis.Text = "";
            txtCena.Text = "";
        }

        private void gridPredmeta_Click(object sender, EventArgs e)
        {
            if (gridPredmeta.SelectedRows.Count == 0)
                return;

            int izabraniPredmet = gridPredmeta.SelectedRows[0].Index;
            Int32 id = (Int32)gridPredmeta["ID", izabraniPredmet].Value;

            Predmet predmet = rdl.GetItem(id);

            txtNaziv.Text = predmet.Naziv;
            txtOpis.Text = predmet.Opis;
            txtCena.Text = predmet.Cena;
        }

        private void FormPredmet_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void txtCena_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }
    }
}
